# Démonstration Départements et Notaires

## Exemple avec aides multiples et communication de la créance

Faire une recherche sur :

1. Date de décès (si activation de la créance) : `16/06/2018`
2. Lieu de décès : Lille
3. Date de l'acte de décès : `16/06/2018`
4. Prénom : `Mohamed`
5. Nom d'usage : `Zadari`
6. Date de naissance : `29/01/1951`

Résultat attendu : personne **connue**

Contenu du mail :

* `5 aides non récupérables` + `1 aide récupérable`
* Montant de la créance : `66987,46€`
